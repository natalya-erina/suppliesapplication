﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SuppliesApplication.DAL;
using SuppliesApplication.Models;
using System.Data.Entity.Infrastructure;
using static System.Collections.Specialized.NameObjectCollectionBase;

namespace SuppliesApplication.Controllers
{
    public class ShopsController : Controller
    {
        private SuppliesContext db = new SuppliesContext();

        // GET: Shops
        public ActionResult Index()
        {
            return View(db.Shops.ToList());
        }


        // POST: Shops
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index([Bind(Include ="Name")] Shop shop)
        {
            List<Shop> filteredShops = new List<Shop>();
            string filter = Request.Form.GetValues("ShopName")[0];
            DbSet allShops = db.Shops;
            foreach (Shop curShop in allShops)
            {
                if (curShop.Name.ToUpper().StartsWith(filter.ToUpper()))
                {
                    filteredShops.Add(curShop);
                }
            }
            return View(filteredShops);
        }

        // GET: Shops/Create
        public ActionResult Create()
        {
            return View(new ShopSuppliers(new Shop(), db.Suppliers.ToList()));
        }

        private List<bool> getCheckBoxesValues()
        {
            string[] checkBoxes = Request.Form.GetValues("item.IsChosen");
            List<bool> isChosen = new List<bool>();
            for (int i = 0; i < checkBoxes.Length; i++)
            {
                isChosen.Add(bool.Parse(checkBoxes[i]));
                if (checkBoxes[i] == "true")
                    i++;
            }
            return isChosen;
        }

        private List<int> getIDs()
        {
            string[] stringIDs = Request.Form.GetValues("item.Supplier.SupplierID");
            List<int> IDs = new List<int>();
            for (int i = 0; i < stringIDs.Length; i++)
                IDs.Add(int.Parse(stringIDs[i]));
            return IDs;
        }

        // POST: Shops/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Name,Address,Phone")] Shop shop)
        {
            if (shop == null)
            {
                return HttpNotFound();
            }
            if (shop.Name != null && shop.Phone != null && shop.Address != null)
            {
                List<bool> isChosen = getCheckBoxesValues();
                List<int> IDs = getIDs();

                shop.Suppliers = new List<Supplier>();

                for (int i = 0; i < isChosen.Count; i++)
                {
                    if (isChosen[i])
                    {
                        Supplier chosenSupplier = db.Suppliers.Find(IDs[i]);
                        shop.Suppliers.Add(chosenSupplier);
                        chosenSupplier.Shops.Add(shop);
                    }
                }
                db.Shops.Add(shop);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(new ShopSuppliers(shop, db.Suppliers.ToList()));
        }

        private ShopSuppliers CreateShopSuppliers(Shop shop)
        {
            List<SupplierViewModel> AllSuppliers = new List<SupplierViewModel>();
            DbSet Suppliers = db.Suppliers;
            foreach (Supplier supplier in Suppliers)
            {
                AllSuppliers.Add(new SupplierViewModel(supplier));
            }

            foreach (SupplierViewModel supplier in AllSuppliers)
            {
                foreach (Supplier chosenSupplier in shop.Suppliers)
                {
                    if (chosenSupplier.SupplierID == supplier.Supplier.SupplierID)
                    {
                        supplier.IsChosen = true;
                    }
                }
            }
            return new ShopSuppliers(shop, AllSuppliers);
        }

        // GET: Shops/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Shop shop = db.Shops.Find(id);
            if (shop == null)
            {
                return HttpNotFound();
            }
            
            return View(CreateShopSuppliers(shop));
        }

        // POST: Shops/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name,Address,Phone")] Shop shop)
        {
            if (shop == null)
            {
                return HttpNotFound();
            }
            if (shop.Name != null && shop.Address != null && shop.Phone != null)
            {
                List<bool> isChosen = getCheckBoxesValues();
                List<int> IDs = getIDs();

                DbSet suppliers = db.Suppliers;
                db.Entry(shop).State = EntityState.Modified;
                shop = db.Shops.Find(shop.ID);
                if (shop.Suppliers == null)
                    shop.Suppliers = new List<Supplier>();
                for (int i = 0; i < isChosen.Count; i++)
                {
                    Supplier supplier = db.Suppliers.Find(IDs[i]);
                    if (isChosen[i])
                    {
                        if (!shop.Suppliers.Contains(supplier))
                            shop.Suppliers.Add(supplier);
                        if (!supplier.Shops.Contains(shop))
                            supplier.Shops.Add(shop);
                    }
                    else
                    {
                        
                        if (shop.Suppliers.Contains(supplier))
                            shop.Suppliers.Remove(supplier);
                        if (supplier.Shops.Contains(shop))
                            supplier.Shops.Remove(shop);
                    }
                }
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(CreateShopSuppliers(db.Shops.Find(shop.ID)));
        }

        // GET: Shops/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Shop shop = db.Shops.Find(id);
            if (shop == null)
            {
                return HttpNotFound();
            }
            return View(shop);
        }

        // POST: Shops/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Shop shop = db.Shops.Find(id);
            db.Shops.Remove(shop);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: Shops/Suppliers
        public ActionResult Suppliers(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Shop shop = db.Shops.Find(id);
            if (shop == null)
            {
                return HttpNotFound();
            }
            return View(new ShopSuppliers(shop, shop.Suppliers));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
