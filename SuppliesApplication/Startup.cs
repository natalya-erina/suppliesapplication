﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SuppliesApplication.Startup))]
namespace SuppliesApplication
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
