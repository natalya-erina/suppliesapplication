﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SuppliesApplication.Models
{
    public class Shop
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }

        public virtual ICollection<Supplier> Suppliers { get; set; }
    }
}