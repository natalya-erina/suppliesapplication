﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SuppliesApplication.Models
{
    public class SupplierViewModel
    {
        public SupplierViewModel(Supplier aSupplier)
        {
            Supplier = aSupplier;
            IsChosen = false;
        }

        public SupplierViewModel(Supplier aSupplier, bool aIsChosen)
        {
            Supplier = aSupplier;
            IsChosen = aIsChosen;
        }

        public SupplierViewModel() { }

        public Supplier Supplier { get; set; }
        public bool IsChosen { get; set; }
    }

    public class ShopSuppliers
    {
        public ShopSuppliers(Shop aShop, List<SupplierViewModel> aSuppliers)
        {
            Shop = aShop;
            Suppliers = new List<SupplierViewModel>();
            foreach (SupplierViewModel svm in aSuppliers)
                Suppliers.Add(svm);
        }

        public ShopSuppliers(Shop aShop, ICollection<Supplier> aSuppliers)
        {
            Shop = aShop;
            Suppliers = new List<SupplierViewModel>();
            foreach (Supplier supplier in aSuppliers)
            {
                Suppliers.Add(new SupplierViewModel(supplier));
            }
        }

        public ShopSuppliers() { }
        public Shop Shop { get; set; }
        public List<SupplierViewModel> Suppliers { get; set; }
    }
}