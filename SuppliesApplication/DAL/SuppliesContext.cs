﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using SuppliesApplication.Models;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace SuppliesApplication.DAL
{
    public class SuppliesContext : DbContext
    {
        public SuppliesContext() : base("SuppliesContext") { }

        public DbSet<Shop> Shops { get; set; }
        public DbSet<Supplier> Suppliers { get; set; }
        //public DbSet<Supply> Supplies { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}