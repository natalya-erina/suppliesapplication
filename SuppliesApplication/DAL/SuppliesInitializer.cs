﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using SuppliesApplication.Models;

namespace SuppliesApplication.DAL
{
    public class SuppliesInitializer : DropCreateDatabaseIfModelChanges<SuppliesContext>
    {
        protected override void Seed(SuppliesContext context)
        {
            var shops = new List<Shop>
            {
                new Shop {Name="Пятерочка", Address="ул. Артамонова, 14", Phone="123456"},
                new Shop {Name="Эльдорадо", Address="Ленинский проспект, 120", Phone="657890"},
                new Shop {Name="Карусель", Address="Лениский проспект, 120", Phone="794673"},
                new Shop {Name="Спортмастер", Address="ул. Кольцоская, 35", Phone="234598"}
            };

            var suppliers = new List<Supplier>
            {
                new Supplier { Name="ТРИУМФ", ContactPerson="Иванов П.А.", City="Москва", Address="ул. Азовская, 23", Phone="123456"},
                new Supplier { Name="BIFLEX", ContactPerson="Петров В.Н.", City="Санкт-Петербург", Address="ул. Слободская, 14", Phone="657890"},
                new Supplier { Name="Русский продукт", ContactPerson="Михайлов Н.Д.", City="Воронеж", Address="ул. Комиссаржевской, 5", Phone="794673"},
                new Supplier { Name="ЛФБ", ContactPerson="Жуков Р.Л.", City="Краснодар", Address="ул. Ленина, 29", Phone="234598"},
                new Supplier { Name="Юнитред", ContactPerson="Иванов К.П.", City="Москва", Address="ул. Таежная, 9", Phone="124849"},
                new Supplier { Name="Мегаторг", ContactPerson="Зайцева А.Н.", City="Москва", Address="ул. Циолковского, 56", Phone="987643"},
                new Supplier { Name="Колибри", ContactPerson="Сидоров А.И.", City="Воронеж", Address="ул. Ленина, 11", Phone="763456"},
                new Supplier { Name="Good-Food", ContactPerson="Орлов Е.О.", City="Санкт-Петербург", Address="ул. Средняя, 29", Phone="243561"},
                new Supplier {Name="Меридиан", ContactPerson="Федоров В.Д.", City="Ижевск", Address="ул. Суворова, 10, ", Phone="956744"},
                new Supplier { Name="Норд", ContactPerson="Попова И.И.", City="Белгород", Address="ул. Советская, 110", Phone="533565"},
                new Supplier { Name="НТЦ", ContactPerson="Кузнецов П.Г.", City="Воронеж", Address="ул. Никитина, 2", Phone="323431"}
            };          

            int i = 0;
            foreach (Shop shop in shops)
            {
                shop.Suppliers = suppliers.GetRange(i, 2);
                i += 2;
            }

            foreach (Shop shop in shops)
            {
                if (shop.Suppliers != null)
                {
                    foreach (Supplier supplier in shop.Suppliers)
                    {
                        if (supplier.Shops == null)
                            supplier.Shops = new List<Shop>();
                        supplier.Shops.Add(shop);
                    }
                }
            }

            shops.ForEach(s => context.Shops.Add(s));
            //context.SaveChanges();

            suppliers.ForEach(s => context.Suppliers.Add(s));
            context.SaveChanges();

            //var supplies = new List<Supply>
            //{
            //    new Supply {ShopID = 1, SupplierID = 12, Date = DateTime.Parse("2016-09-07")},
            //    new Supply {ShopID = 1, SupplierID = 17, Date = DateTime.Parse("2016-09-09")},
            //    new Supply {ShopID = 1, SupplierID = 23, Date = DateTime.Parse("2016-09-01")},
            //    new Supply {ShopID = 2, SupplierID = 12, Date = DateTime.Parse("2016-09-07")},
            //    new Supply {ShopID = 2, SupplierID = 15, Date = DateTime.Parse("2016-09-05")},
            //    new Supply {ShopID = 3, SupplierID = 19, Date = DateTime.Parse("2016-09-13")},
            //    new Supply {ShopID = 3, SupplierID = 20, Date = DateTime.Parse("2016-09-17")},
            //    new Supply {ShopID = 4, SupplierID = 21, Date = DateTime.Parse("2016-09-11")}
            //};

            //supplies.ForEach(s => context.Supplies.Add(s));
            //context.SaveChanges();
        }
    }
}